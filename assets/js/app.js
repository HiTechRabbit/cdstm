async function getStatuses() {
	const statusSection = document.getElementById('status')

	const response = await fetch('https://api.cdstm.ch/incidents', {
	    method: 'GET',
	    mode: 'cors',
	    headers:{
	      'Content-Type': 'application/json'
	    }
	})	

	const json = await response.json()

	for(let service in json.status) {
		let newService = document.createElement('a')
		newService.href = 'http://status.cdstm.ch'

		let state = json.status[service].state

		let title = document.createElement('h4')
		let icon = document.createElement('i')

		title.innerText = service
		icon.className = 'fa fa-check fa-1x'
		if (state =! 'operationnal') {
			icon.className = 'fa fa-exclamation-triangle fa-1x'
		}
		
		newService.appendChild(title)
		newService.appendChild(icon)
		statusSection.appendChild(newService)
	}
}

window.addEventListener("load", getStatuses)